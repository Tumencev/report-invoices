<?php
/** @noinspection AutoloadingIssuesInspection */

use Bitrix\Currency\CurrencyTable;
use Bitrix\Main\Context;
use Bitrix\Main\Grid;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\Filter;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\UserTable;
use Bitrix\Main\Web\Json;
use Pusk\Orm\InvoiceTable;


class reportStatement extends CBitrixComponent
{
    /**
     *  Выполняем какието вычисления
     */
    const GRID_ID = 'INVOICE_TYPE_REPORT';

    private static $headers;
    private static $filterFields;
    private static $filterPresets;
    private static $amountNum;


    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        $params['filter'] = array('=BASE' => 'Y');
        $optionCurrency = CurrencyTable::getList($params)->fetch();
        self::$amountNum = 2;
        $currencyNum = '(' . $optionCurrency['CURRENCY'] . ')';

        self::$headers = [
            ['id' => 'RESPONSIBLE_NAME', 'name' => Loc::getMessage('FULL_NAME_MANAGER'), 'sort' => 'RESPONSIBLE_NAME', 'default' => true],
            ['id' => 'COUNT_SUC_STATEMENTS', 'name' => Loc::getMessage('COUNT_PAYED'), 'sort' => 'COUNT_SUC_STATEMENTS', 'default' => true],
            ['id' => 'FULL_PRICE', 'name' => Loc::getMessage('FULL_PRICE_GET') . $currencyNum, 'sort' => 'FULL_PRICE', 'default' => true],
            ['id' => 'MIDDLE_PRICE', 'name' => Loc::getMessage("MIDDLE_PRICE") . $currencyNum, 'sort' => 'MIDDLE_PRICE', 'default' => true],
            ['id' => 'PRICE_BROKEN_STATEMENTS', 'name' => Loc::getMessage("BROKE_PRICE") . $currencyNum, 'sort' => 'PRICE_BROKEN_STATEMENTS', 'default' => true],
            ['id' => 'COUNT_BROKEN_STATEMENTS', 'name' => Loc::getMessage("COUNT_BROKE"), 'sort' => 'COUNT_BROKEN_STATEMENTS', 'default' => true],

        ];

        self::$filterFields = [
            ['id' => 'DATE_INSERT',
                'name' => Loc::getMessage("DATE_CREATE"),
                'type' => 'date',
                'params' => [
                    'context' => 'DATE_INSERT',
                    'multiple' => 'Y',
                    'contextCode' => '',
                    'enableAll' => 'N',
                    'enableSonetgroups' => 'N',
                    'allowEmailInvitation' => 'N',
                    'allowSearchEmailUsers' => 'N',
                    'departmentSelectDisable' => 'Y',
                    'isNumeric' => 'Y',
                    'prefix' => ''
                ],
                'default' => true],
            ['id' => 'PAY_VOUCHER_DATE',
                'name' => Loc::getMessage("DATE_PAYED"),
                'type' => 'date',
                'params' => [
                    'context' => 'PAY_VOUCHER_DATE',
                    'multiple' => 'Y',
                    'contextCode' => '',
                    'enableAll' => 'N',
                    'enableSonetgroups' => 'N',
                    'allowEmailInvitation' => 'N',
                    'allowSearchEmailUsers' => 'N',
                    'departmentSelectDisable' => 'Y',
                    'isNumeric' => 'Y',
                    'prefix' => ''
                ],
                'default' => true],
            ['id' => 'DATE_STATUS',
                'name' => Loc::getMessage("DATE_UPDATE"),
                'type' => 'date',
                'params' => [
                    'context' => 'DATE_STATUS',
                    'multiple' => 'Y',
                    'contextCode' => '',
                    'enableAll' => 'N',
                    'enableSonetgroups' => 'N',
                    'allowEmailInvitation' => 'N',
                    'allowSearchEmailUsers' => 'N',
                    'departmentSelectDisable' => 'Y',
                    'isNumeric' => 'Y',
                    'prefix' => ''
                ],
                'default' => true],

        ];
        self::$filterPresets = array();

    }

    public function executeComponent()
    {

        $grid = new Grid\Options(self::GRID_ID);

        $gridSort = $grid->getSorting();

        $gridFilter = new Filter\Options(self::GRID_ID, self::$filterPresets);
        $filter = $gridFilter->getFilter(self::$filterFields);
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $gridNav = $grid->GetNavParams();
        $pager = new PageNavigation('invoice_uf_report');
        $pager->setPageSize($gridNav['nPageSize']);
        if ($request->offsetExists('page')) {
            $currentPage = $request->get('page');
            $pager->setCurrentPage($currentPage > 0 ? $currentPage : $pager->getPageCount());
        } else {
            $pager->setCurrentPage(1);
        }

        if (empty($gridSort)) {
            $gridSort = array('RESPONSIBLE_NAME' => 'ASC');
        }

        $reportData = $this->getReport($pager, [
            'filter' => $this->parseFilter($filter),
            'order' => $gridSort
        ]);

        $this->processServiceActions($reportData['COUNT_TOTAL']);
        $this->arResult = [
            'SORT' => $gridSort,
            'COUNTS' => $reportData['COUNTS'],
            'GRID_ID' => self::GRID_ID,
            'REPORT_DATA' => $reportData['REPORT_DATA'],
            'HEADERS' => self::$headers,
            'FILTER' => self::$filterFields,
            'FILTER_PRESETS' => self::$filterPresets,
            'PAGINATION' => [
                'PAGE_NUM' => $pager->getCurrentPage(),
                'ENABLE_NEXT_PAGE' => $pager->getCurrentPage() < $pager->getPageCount(),
                'URL' => $request->getRequestedPage(),
            ],
            'ENABLE_LIVE_SEARCH' => false,
            'DISABLE_SEARCH' => true,
        ];
        try {
            $this->includeComponentTemplate();
        } catch (\Exception $exception) {
            ShowError($exception->getMessage());
        }
    }

    public function getReport($pager, $params)
    {
        $arParams['runtime'] = [
            new \Bitrix\Main\Entity\ExpressionField('COUNT_SUC_STATEMENTS',
                'SUM(IF(STATUS_ID="P",1,0))'),
            new \Bitrix\Main\Entity\ExpressionField('FULL_PRICE',
                'SUM(IF(STATUS_ID="P",PRICE,0))'),
            new \Bitrix\Main\Entity\ExpressionField('MIDDLE_PRICE',
                'AVG(CASE WHEN STATUS_ID="P" then PRICE else null end)'),
            new \Bitrix\Main\Entity\ExpressionField('COUNT_BROKEN_STATEMENTS',
                'SUM(IF(STATUS_ID="D",1,0))'),
            new \Bitrix\Main\Entity\ExpressionField('PRICE_BROKEN_STATEMENTS',
                'SUM(IF(STATUS_ID="D",PRICE,0))'),
            'RESPONSIBLE_USER' => [
                'data_type' => UserTable::class,
                'reference' => [
                    '=this.RESPONSIBLE_ID' => 'ref.ID',
                ],
                'join_type' => 'inner'
            ],
            new \Bitrix\Main\Entity\ExpressionField('RESPONSIBLE_NAME', "IF(LENGTH(pusk_orm_invoice_responsible_user.NAME)>0,
		CONCAT_WS(' ',pusk_orm_invoice_responsible_user.NAME,pusk_orm_invoice_responsible_user.LAST_NAME), pusk_orm_invoice_responsible_user.LOGIN)"),

        ];
        $arParams['select'] = [
            'RESPONSIBLE_NAME',
            'RESPONSIBLE_ID',
            'COUNT_SUC_STATEMENTS',
            'FULL_PRICE',
            'MIDDLE_PRICE',
            'COUNT_BROKEN_STATEMENTS',
            'PRICE_BROKEN_STATEMENTS',

        ];
        $arParams['group'] = array('RESPONSIBLE_NAME',
            'RESPONSIBLE_ID',
        );
        $arParams['count_total'] = true;
        if ($pager !== null) {
            $arParams['limit'] = $pager->getLimit();
            $arParams['offset'] = $pager->getOffset();
            $arParams['order'] = $params['order']['sort'];
        }
        $arParams['filter'] = $params['filter'];

        $dbreq = InvoiceTable::getList($arParams);

        $countTotal = $dbreq->getCount();
        if ($pager !== null) {
            $pager->setRecordCount($countTotal);
        }
        $countPayed = 0;
        $countBroke = 0;
        $dataResult = [];
        foreach ($dbreq->fetchAll() as $row) {
            $countPayed += $row['COUNT_SUC_STATEMENTS'];
            $countBroke += $row['COUNT_BROKEN_STATEMENTS'];
            $dataResult[] = [
                'data' => [
                    'ID' => "{$row['RESPONSIBLE_ID']}",
                    'RESPONSIBLE_NAME' => "{$row['RESPONSIBLE_NAME']}",
                    'COUNT_SUC_STATEMENTS' => "{$row['COUNT_SUC_STATEMENTS']}",
                    'FULL_PRICE' => number_format($row['FULL_PRICE'], self::$amountNum, '.', ''),
                    'MIDDLE_PRICE' => number_format($row['MIDDLE_PRICE'], self::$amountNum, '.', ''),
                    'PRICE_BROKEN_STATEMENTS' => number_format($row['PRICE_BROKEN_STATEMENTS'], self::$amountNum, '.', ''),
                    'COUNT_BROKEN_STATEMENTS' => "{$row['COUNT_BROKEN_STATEMENTS']}"
                ]
            ];
        }

        return [
            'COUNT_TOTAL' => $countTotal,
            'REPORT_DATA' => $dataResult,
            'COUNTS' => [
                'PAYED' => $countPayed,
                'BROKE' => $countBroke,
            ]
        ];
    }

    public function parseFilter(array $gridFilterValues)
    {
        $filter = [];

        if (!empty($gridFilterValues)) {

            $date_start = $gridFilterValues['DATE_INSERT_from'];
            $date_end = $gridFilterValues['DATE_INSERT_to'];

            if ($date_start !== null) {
                $filter['>=DATE_INSERT'] = $date_start;
            }
            if ($date_end !== null) {
                $filter['<=DATE_INSERT'] = $date_end;
            }
            if (!empty($gridFilterValues['DATE_INSERT'])) {
                foreach ($gridFilterValues['DATE_INSERT'] as &$item) {
                    $item = str_replace('U', '', $item);
                }
                unset($item);
                $filter['DATE_INSERT'] = $gridFilterValues['DATE_INSERT'];

            }
            if (!empty($gridFilterValues['RESPONSIBLE_ID'])) {
                $filter['RESPONSIBLE_ID'] = str_replace('U', '', $gridFilterValues['RESPONSIBLE_ID']);
            }


            $date_start = $gridFilterValues['PAY_VOUCHER_DATE_from'];
            $date_end = $gridFilterValues['PAY_VOUCHER_DATE_to'];

            if ($date_start !== null) {
                $filter['>=PAY_VOUCHER_DATE'] = $date_start;
            }
            if ($date_end !== null) {
                $filter['<=PAY_VOUCHER_DATE'] = $date_end;
            }
            if (!empty($gridFilterValues['PAY_VOUCHER_DATE'])) {
                foreach ($gridFilterValues['PAY_VOUCHER_DATE'] as &$item) {
                    $item = str_replace('U', '', $item);
                }
                unset($item);
                $filter['PAY_VOUCHER_DATE'] = $gridFilterValues['PAY_VOUCHER_DATE'];

            }

            $date_start = $gridFilterValues['DATE_STATUS_from'];
            $date_end = $gridFilterValues['DATE_STATUS_to'];

            if ($date_start !== null) {
                $filter['>=DATE_STATUS'] = $date_start;
            }
            if ($date_end !== null) {
                $filter['<=DATE_STATUS'] = $date_end;
            }
            if (!empty($gridFilterValues['DATE_STATUS'])) {
                foreach ($gridFilterValues['DATE_STATUS'] as &$item) {
                    $item = str_replace('U', '', $item);
                }
                unset($item);
                $filter['DATE_STATUS'] = $gridFilterValues['DATE_STATUS'];

            }
        }
        return $filter;
    }

    private function processServiceActions($count)
    {

        global $APPLICATION;

        if (!check_bitrix_sessid()) {
            return;
        }
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $params = $request->get('PARAMS');
        if (empty($params['GRID_ID']) || $params['GRID_ID'] !== self::GRID_ID) {
            return;
        }
        $action = $request->get('ACTION');
        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');
        switch ($action) {
            case 'GET_ROW_COUNT':
                echo Json::encode(array(
                    'DATA' => array(
                        'TEXT' => Loc::getMessage("COUNT_ALL") . $count
                    )
                ));
                break;

            default:
                break;
        }
        require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php';
        die;
    }

}