<?php

use Bitrix\Main\Page\Asset;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @var CBitrixComponentTemplate $this */
$rows = array();
$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');
\Bitrix\Main\UI\Extension::load('ui.buttons');
CJSCore::Init(array('jquery'));
CJSCore::Init(array('amcharts', 'amcharts_pie'));

$rows = array();
foreach ($arResult['REPORT_DATA'] as $item) {
    $rows[] = [
        'id' => $item['data']['ID'],
        'data' => $item['data'],
        'columns' => [
            $item['data']
        ]
    ];
}

$gridManagerId = $arResult['GRID_ID'];
$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.grid',
    'titleflex',
    [
        'GRID_ID' => 'INVOICE_TYPE_REPORT',
        'HEADERS' => $arResult['HEADERS'],
        'ROWS' => $rows,
        'PAGINATION' => $arResult['PAGINATION'],
        'SORT' => '',
        'FILTER' => $arResult['FILTER'],
        'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
        'IS_EXTERNAL_FILTER' => false,
        'ENABLE_LIVE_SEARCH' => $arResult['ENABLE_LIVE_SEARCH'],
        'DISABLE_SEARCH' => true,
        'ENABLE_ROW_COUNT_LOADER' => true,
        'AJAX_ID' => '',
        'AJAX_OPTION_JUMP' => 'N',
        'AJAX_OPTION_HISTORY' => 'N',
        'AJAX_LOADER' => null,
        'ACTION_PANEL' => [],
        'EXTENSION' => [
            'ID' => $gridManagerId,
            'CONFIG' => [
                'ownerTypeName' => 'STORE',
                'gridId' => $arResult['GRID_ID'],
                'serviceUrl' => $arResult['SERVICE_URL'],
            ],
        ],
        'SHOW_ROW_CHECKBOXES' => false,
        'SHOW_SELECTED_COUNTER' => false,

    ],
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);
?>


