<?php
$MESS['FULL_NAME_MANAGER'] = 'ФИО менеджера';
$MESS['COUNT_PAYED'] = 'Кол-во оплаченных счетов';
$MESS['FULL_PRICE_GET'] = 'Всего получено';
$MESS["MIDDLE_PRICE"] = "Средняя стоимость";
$MESS["BROKE_PRICE"] = "Сумма забракованных";
$MESS["COUNT_BROKE"] = "Кол-во забракованных";
$MESS["DATE_CREATE"] = "Дата создания";
$MESS["DATE_PAYED"] = "Дата оплаты";
$MESS["DATE_UPDATE"] = "Дата изменения статуса";
$MESS["COUNT_ALL"] = "Всего: ";
