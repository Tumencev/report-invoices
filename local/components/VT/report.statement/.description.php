<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentDescription = [
    'NAME' => 'Report Statement',
    'DESCRIPTION' => '',
    'SORT' => 10,
    'CACHE_PATH' => 'Y',
    'PATH' => [
        'ID' => 'vtrep',
        'NAME' => 'vtrep',
        'SORT' => 10,
    ],
    'COMPLEX' => 'N',
];