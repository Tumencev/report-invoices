<?php

namespace Pusk\Orm;


use Bitrix\Main\Entity\DataManager;

class InvoiceTable extends DataManager
{
    public static function getTableName()
    {
        return 'b_crm_invoice';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),

            'PRICE' => array(
                'data_type' => 'float',
            ),
            'STATUS_ID' => array(
                'data_type' => 'integer',
            ),
            'PAY_VOUCHER_DATE' => array(
                'data_type' => 'date',
            ),
            'DATE_INSERT' => array(
                'data_type' => 'date',
            ),
            'RESPONSIBLE_ID' => array(
                'data_type' => 'string',
            ),

            'DATE_STATUS' => array(
                'data_type' => 'datetime',
            )
        );
    }
}